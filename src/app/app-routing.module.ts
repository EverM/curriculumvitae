import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactoComponent } from './components/contacto/contacto.component';

import { ServiciosComponent } from './components/servicios/servicios.component';
import { CurriculumComponent } from './components/curriculum/curriculum.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { SobreMiComponent } from './components/sobre-mi/sobre-mi.component';

const routes: Routes = [

  {path:'servicios',component:ServiciosComponent},
  {path:'contacto', component:ContactoComponent},
  {path:'curriculum', component:CurriculumComponent},
  {path:'agenda', component:AgendaComponent},
  {path:'sobreMi',component:SobreMiComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes) ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
